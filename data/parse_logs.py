import os 
import re
import sys
import math
import logging
import statistics
import matplotlib.pyplot as plt
from datetime import datetime

def process_log_file(filename):
    """
    Output number of failure per minute in this log file by num_failed / (last_fail_time - first_fail_time)
    """
    with open(filename, "r") as file:
        content = file.read()
    lines = content.split("\n")
    start_time = None
    end_time = None
    total_fails = 0
    ts_to_fail = {}

    for line in lines:
        # collect last joined timestamp
        if "failed" not in line:
            res = re.match(r"(.*): Node fa20-cs425-g51-(\d\d).cs.illinois.edu:.* joined the group.", line)
            if res is None:
                continue
            ts = datetime.strptime(res.groups()[0], '%c')
            start_time = ts
            continue

        res = re.match(r"(.*): Node fa20-cs425-g51-(\d\d).cs.illinois.edu:.* failed.", line)
        ts = datetime.strptime(res.groups()[0], '%c')
        if start_time is None:
            start_time = ts
        end_time = ts

        if ts in ts_to_fail:
            ts_to_fail[ts] += 1
        else:
            ts_to_fail[ts] = 0
        total_fails += 1

    for ts, fail_num in ts_to_fail.items():
        # very unlikely that this many nodes failed at once,
        # it's more likely that this node itself is marked as failed by all other nodes, 
        # (because of consecutive dropped message from this node)
        # and no longer receives any message. don't want to count everyone else who didn't fail as failed
        if fail_num > 5:
            print(f"Sanitizing {filename}")
            total_fails -= fail_num - 1
    
    delta_m = (end_time - start_time).total_seconds() / 60
    if delta_m == 0:
        print(f"delta is 0 for {filename}")
        return math.inf

    return total_fails / delta_m

if __name__ == "__main__":
    pwd = os.getcwd()
    if "data" not in pwd:
        os.chdir("./data/")

    # { percent: [list of fail per minute for this percent] }
    gossip_data = {}
    all2all_data = {}

    dirs = os.listdir()

    for fn in sorted(dirs):
        res = re.match(r"(\d\d)_(all2all|gossip)_(\d+)%.log", fn)
        if not res:
            continue
        
        groups = res.groups()
        node_num, protocol, percent = groups[0], groups[1], int(groups[2])

        if protocol == "gossip":
            if percent not in gossip_data:
                gossip_data[percent] = []
            gossip_data[percent].append(process_log_file(fn))
        elif protocol == "all2all":
            if percent not in all2all_data:
                all2all_data[percent] = []
            all2all_data[percent].append(process_log_file(fn))

    (all2all_percent, all2all_fpm) = zip(*sorted(all2all_data.items(), key=lambda x: x[0]))
    (gossip_percent, gossip_fpm) = zip(*sorted(gossip_data.items(), key=lambda x: x[0]))

    a2a_mean = [statistics.mean(x) for x in all2all_fpm]
    a2a_std = [statistics.stdev(x) for x in all2all_fpm]
    gossip_mean = [statistics.mean(x) for x in gossip_fpm]
    gossip_std = [statistics.stdev(x) for x in gossip_fpm]
    
    fig, (ax1, ax2) = plt.subplots(1, 2, sharex=True, figsize=(8, 4))

    ax1.errorbar(all2all_percent, a2a_mean, a2a_std, fmt='ok', lw=1)
    ax1.title.set_text("All2All")
    ax1.set_ylabel("# of false positives per minute")
    ax1.set_xlabel("message drop rate (%)")

    ax2.errorbar(gossip_percent, gossip_mean, gossip_std, fmt='ok', lw=1)
    ax2.title.set_text("Gossip")
    ax2.set_ylabel("# of false positives per minute")
    ax2.set_xlabel("message drop rate (%)")

    plt.subplots_adjust(wspace=0.4)

    plt.tight_layout()
    plt.savefig("figure.png")
    plt.show()
