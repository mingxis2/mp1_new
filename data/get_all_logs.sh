set -e

for i in $(seq -f "%02g" 1 10)
do
for j in 01 04 08 12 16 20
do
for protocol in gossip all2all
do
  scp xc14@fa20-cs425-g51-${i}.cs.illinois.edu:~/mp1/${protocol}_${j}%.log ~/gitRepos/CS425/mp1/data/${i}_${protocol}_${j}%.log
done
done
done